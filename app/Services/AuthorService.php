<?php


namespace App\Services;


use App\Traits\ConsumeExternalServices;

class AuthorService
{
    use ConsumeExternalServices;

    /**
     * The base uri to consume the authors service
     * @var string
     */
    public $baseUri;

    public function __construct()
    {
        $this->baseUri = config('services.authors.base_url');
    }

    /**
     * Get the full list of authors from the author services
     */
    public function index(){
        return $this->performRequest('GET', '/authors');
    }

    /**
     * Create an Author
     * @param $request
     * @return string
     */
    public function store($request)
    {
        return $this->performRequest('POST', '/authors/', $request->all());
    }

    /**
     * Get the full list of authors from the author services
     */
    public function show($author){
        return $this->performRequest('GET', '/authors/'.$author);
    }
}
