<?php


namespace App\Services;


use App\Traits\ConsumeExternalServices;

class BookService
{
    use ConsumeExternalServices;

    protected $baseUri;

    public function __construct()
    {
        $this->baseUri = config('services.books.base_url');
    }

    public function index()
    {
        return $this->performRequest('GET','/books');
    }

    public function store($request)
    {
        return $this->performRequest('POST', '/books', $request->all());
    }

    public function show($book)
    {
        return $this->performRequest('GET', '/books/'.$book);
    }
}
