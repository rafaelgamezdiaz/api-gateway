<?php

namespace App\Http\Controllers;

use App\Services\AuthorService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Response;

class AuthorController extends Controller
{
    use ApiResponser;

    /**
     * The service to consume authors microservices
     * @var AuthorService
     */
    public $authorService;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    /**
     * Returns the list of authors
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->successResponse($this->authorService->index());
    }

    /**
     * Create one new author
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->successResponse($this->authorService->store($request), Response::HTTP_CREATED);
    }

    /**
     * Obtain and Show one author
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($author)
    {
        return $this->successResponse($this->authorService->show($author));
    }

    /**
     * Update an existing author
     * @return Resource
     */
    public function update(Request $request, $author)
    {
    }

    /**
     * Remove an existing author
     * @return Resource
     */
    public function destroy($author)
    {
    }

}

