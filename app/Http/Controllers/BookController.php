<?php

namespace App\Http\Controllers;

use App\Services\BookService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookController extends Controller
{
    use ApiResponser;

    /**
     * The service to consume the book microsystem
     * @var BookService
     */
    public $bookService;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }

    public function index()
    {
        return $this->successResponse($this->bookService->index());
    }

    public function store(Request $request)
    {
        return $this->successResponse($this->bookService->store($request), Response::HTTP_CREATED);
    }

    public function show($book)
    {
        return $this->successResponse($this->bookService->show($book));
    }

    public  function update(Request $request, $book){
    }

    public function destroy($book){
    }
}

